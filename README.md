# My Personal Awesomes
A list with projects and repositories that I find awesome!  
Emoji explanations are found at the bottom of this file.

## Contents

### Programming
- [Node(JS) Libaries](#nodejs-libaries)
- [IDE's / Text-editors](#ides-text-editors)
- [Tools / Utilities](#tools-utilities)
- [Version Control](#version-control)

### Software
- [Chat Platforms](#chat-platforms)
- [CLI](#cli)
- [Utilities](#utilities)

### Games
- [Software](#software)
- [Check these out!](#check-these-out)
- [Free to Play Games]()
- [Mods / Modpacks]()

### [Emojis Explained](#emojis-explained)
---

## Programming

### Node(JS) Libaries
- None listed yet.

### IDE's / Text-editors
- [Visual Studio Code ⭐](https://code.visualstudio.com/) - This is my all time favourite text-editor.
- [Visual Studio ⭐](https://visualstudio.microsoft.com/vs/) - And for IDE's this is my all time favourite!
- [Atom](https://atom.io/) - The hackable editor by Github totally deserves a place on my list.
- [Android Studio](https://developer.android.com/studio) - Not many people like this little baby but I find it amazing!

### Tools / Utilities
- [PlatformIO ⭐](https://platformio.org/) - Great for developing boards to use for your smart home.
- [ESPHome ⭐](https://esphome.io/) - Easy to use system to program boards for your smart home!
- [VueJS CLI ⭐](https://cli.vuejs.org/) - Fast en easy creating a VueJS project in seconds.
- [DB Browser for SQLite](https://sqlitebrowser.org/) - Does what it needs to do! =D

### Version Control
- [GitLab ⭐](https://gitlab.com/) - After using GitHub for a long time, I'm glad to be on GitLab now.
- [GitHub](https://github.com/) - Everyone ofcourse knows GitHub, who doesn't? 

## Software

### Chat Platforms
- [Slack ⭐](https://slack.com/) - One of the better chat clients out there.
- [WhatsApp Web ⭐](https://www.whatsapp.com/download/) - Use whatsapp on your desktop.
- [Discord](https://discord.com/) - I have a hate love relation with Discord.
- [Keybase](https://keybase.io/) - A great chat client with a lot of encrypted features.
- [Gitter](https://gitter.im/) - Easy for small chatrooms for your projects.

### CLI
- [Chocolatey ⭐](https://chocolatey.org/) - The Package Manager for Windows.
- [Git ⭐](https://git-scm.com/) - I assume not much explanation is needed.
- [youtube-dl ⭐](https://youtube-dl.org/) - The tool to download from YouTube.
- [Speedtest CLI ⭐](https://www.speedtest.net/apps/cli) - Quickly test your current internet speeds.
- [Winget](https://docs.microsoft.com/en-us/windows/package-manager/winget/) - A package manager from Windows itself, still in early stage.
- [Neofetch](https://github.com/dylanaraps/neofetch) - Get information about your system in no-time!
- [Screenfetch](https://github.com/KittyKatt/screenFetch) - Same as Neofetch but different. 😋

### Utilities
- [Windows PowerToys ⭐](https://github.com/microsoft/PowerToys) - Adds tiling and more tools to Windows
- [ShareX ⭐](https://getsharex.com/) - Screen capture, file sharing and productivity tool
- [Docker Desktop ⭐](https://www.docker.com/products/docker-desktop) - Indispensable!

## Games

### Software
- [Steam ⭐](https://steamcommunity.com/) - Explanation needed?

### Check these out
- [Screeps ⭐](https://screeps.com/) - You like coding in Javascript and Gaming? You need to play this!
- [Slay the Spire ⭐ 💲](https://store.steampowered.com/app/646570/Slay_the_Spire/) - Great to kill time with this Roquelike card game!


## Emojis explained
- ⭐: Items with a star are my top favorite and are definitly worth checking out.
- 💲: Items with the dollar sign are only available when paying for it.